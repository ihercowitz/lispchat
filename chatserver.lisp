; A very simple chat using socket and Telnet by Igor Hercowitz 
; To start the server, load the file on CLISP and then type (start-server <port number>)
; 
; Open a telnet and type <IP> <PORT>
; ex: telnet localhost 9999
;
; To stop the server, you have to kill the process :|
;
; This code was based on this: http://rosettacode.org/wiki/Echo_server#Common_Lisp
;

(defparameter *clients* '())

(defun stop-server (server)
  (format t "Stoping server: ~a:~d~%"
      (socket:socket-server-host server)
      (socket:socket-server-port server))
  
    (socket-server-close server)
)

(defun hello-send-message (socket host port)
    (format socket "[ ~a:~d has joined ]~%" host port)
    (finish-output socket)
)

(defun new-client (socket)
  (multiple-value-bind
    (host port) (socket:socket-stream-peer socket)
    (format t "Connect from ~a:~d~%" host port)
  (push (list socket :input nil) *clients*) 

  (mapcar #'(lambda (client)
                    (hello-send-message (car client) host port)) *clients*))
)

(defun send-message(socket)
  (multiple-value-bind
    (host port) (socket:socket-stream-peer socket)
    
  (let ((line (read-line socket nil nil)))
    (cond
        ((string= "/bye" line)
         (format socket "Good Bye")
         (close-client socket)
         )

    (t 
    (mapcar #'(lambda (client)
                (format (car client) "~a:~d says: ~a~%" host port line)
                (finish-output (car client))
                ) *clients* ))  ))
     
  )
)


(defun close-client (socket)
   (multiple-value-bind
     (host port) (socket:socket-stream-peer socket)
      (format t "Closing connection from ~a:~d~%" host port)  
      (setq *clients* (remove socket *clients* :key #'car))
      (mapcar #'(lambda (client)
                    (format (car client) "~a:~d has left~%" host port)
                    (finish-output (car client))
                    ) *clients*))
      (close socket)
)

(defun start-server (port)
  (let ((server (socket:socket-server port)))
    (format t "Server is listening on ~a:~d~%"
            (socket:socket-server-host server)
            (socket:socket-server-port server))
     (format t "Server ready to accept new clients~%")
     (unwind-protect
     (loop          
       (when (socket:socket-status server 0 1)
         (new-client (socket:socket-accept server
                            :external-format :dos               
                            :buffered t))          
       )

       (when *clients*
         (socket:socket-status *clients* 0 1)
         (mapcar #'(lambda (client)
                     (when (eq :input (cddr client))
                        (send-message (car client)))

                     ) *clients*)
         )
      
     )

    (stop-server server)   
  ))
)
